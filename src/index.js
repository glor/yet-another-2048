let field = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];

function isGameOver() {
    return isGameLost() || isGameWon();
}

function isGameLost() {
    return !field.reduce((prev, curr) => prev.concat(curr)).includes(0);
}

function isGameWon() {
    return field.reduce((prev, curr) => prev.concat(curr)).includes(2048);
}

function placeRandomly() {
    if (isGameLost())
        throw new Error("error. your game developer sucks");
    let x, y;
    do {
        x = Math.floor(Math.random() * 4);
        y = Math.floor(Math.random() * 4);
    } while (field[x][y] !== 0);
    field[x][y] = 2;
}

function round(dir) {
    let changed = shift(dir);
    changed |= add(dir);
    shift(dir);
    if (!isGameOver() && changed) {
        placeRandomly();
    }
    render();
    if (isGameOver())
        document.getElementById('app').innerHTML += `<br/> Game over`;
}

function isEmpty(x, y) {
    return field[x][y] === 0;
}

function selectColor(x) {
    switch (x) {
        case 0:
            return 'lightgrey';
        case 2:
            return 'lightblue';
        case 4:
            return 'lightgreen';
        default:
            return 'lightsalmon';
    }
}

function render() {
    document.getElementById('app').innerHTML =
        field.map((hlist) => {
            return '<div>' + hlist.map((x) => `<span class="block" style="background-color: ${selectColor(x)}">${x}</span>`).join('') + '</div>';
            //return `<div><b class="block"><br/><br/>${hlist.join('</b><b
            // class="block"><br/><br/>')}</b></div>`;
        }).join('');
}

function move(xfrom, yfrom, xto, yto) {
    //console.log(`field[${xto}][${yto}] === field[${xfrom}][${yfrom}]`);
    if (field[xto][yto] === field[xfrom][yfrom])
        field[xto][yto] += field[xfrom][yfrom];
    else
        field[xto][yto] = field[xfrom][yfrom];
    field[xfrom][yfrom] = 0;
}

let direction = {
    up: (x, y) => x === 0 ? {x: x, y: y} : {x: x - 1, y: y},
    down: (x, y) => x === 3 ? {x: x, y: y} : {x: x + 1, y: y},
    left: (x, y) => y === 0 ? {x: x, y: y} : {x: x, y: y - 1},
    right: (x, y) => y === 3 ? {x: x, y: y} : {x: x, y: y + 1}
};


function shift(dir) {
    let changes = false;
    for (let k = 0; k < 16; k++) {
        for (let i = 0; i < field.length; i++) {
            for (let j = 0; j < field[0].length; j++) {
                let {x, y} = direction[dir](i, j);
                if (i === x && j === y || field[i][j] === field[x][y])
                    continue;
                if (isEmpty(x, y)) {
                    move(i, j, x, y);
                    changes = true;
                }
            }
        }
    }
    return changes;
}

function add(dir) {
    let changes = false;
    switch (dir) {
        case 'up':
            for (let i = 0; i < field.length - 1; i++) {
                for (let j = 0; j < field[0].length; j++) {
                    if (field[i][j] === 0)
                        continue;
                    let x = i + 1;
                    let y = j;
                    if (field[i][j] === field[x][y]) {
                        field[i][j] *= 2;
                        field[x][y] = 0;
                        changes = true;
                    }

                }
            }
            break;
        case 'down':
            for (let i = 1; i < field.length; i++) {
                for (let j = 0; j < field[0].length; j++) {
                    if (field[i][j] === 0)
                        continue;
                    let x = i - 1;
                    let y = j;
                    if (field[i][j] === field[x][y]) {
                        field[i][j] *= 2;
                        field[x][y] = 0;
                        changes = true;
                    }

                }
            }
            break;
        case 'left':
            for (let i = 0; i < field.length; i++) {
                for (let j = 0; j < field[0].length - 1; j++) {
                    if (field[i][j] === 0)
                        continue;
                    let x = i;
                    let y = j + 1;
                    if (field[i][j] === field[x][y]) {
                        field[i][j] *= 2;
                        field[x][y] = 0;
                        changes = true;
                    }

                }
            }
            break;
        case 'right':
            for (let i = 0; i < field.length; i++) {
                for (let j = 1; j < field[0].length; j++) {
                    if (field[i][j] === 0)
                        continue;
                    let x = i;
                    let y = j - 1;
                    if (field[i][j] === field[x][y]) {
                        field[i][j] *= 2;
                        field[x][y] = 0;
                        changes = true;
                    }

                }
            }
            break;
    }
    return changes;
}

document.onkeydown = function (e) {
    e = e || window.event;

    if (e.key === 'ArrowUp' || e.key === 'w') {
        round('up');
    } else if (e.key === 'ArrowLeft' || e.key === 'a') {
        round('left');
    } else if (e.key === 'ArrowRight' || e.key === 'd') {
        round('right');
    } else if (e.key === 'ArrowDown' || e.key === 's') {
        round('down');
    }
};

import AlloyFinger from 'alloyfinger';

var af = new AlloyFinger(document, {
    swipe: function (evt) {
        console.log("swipe" + evt.direction);
    }
});

import Hammer from '../hammerjs';

var mc = new Hammer(document, {
    touchAction: 'auto',
    inputClass: Hammer.SUPPORT_POINTER_EVENTS ? Hammer.PointerEventInput : Hammer.TouchInput,
    recognizers: [
        [Hammer.Swipe, {
            direction: Hammer.DIRECTION_HORIZONTAL
        }]
    ]
});

mc.get('swipe').set({direction: Hammer.DIRECTION_ALL});

mc.on("swipeleft swiperight swipeup swipedown", function (ev) {
    switch (ev.type) {
        case 'swipeup':
            round('up');
            break;
        case 'swipeleft':
            round('left');
            break;
        case 'swiperight':
            round('right');
            break;
        case 'swipedown':
            round('down');
            break;
    }
});

placeRandomly();
render();

